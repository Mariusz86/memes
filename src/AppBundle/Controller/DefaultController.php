<?php

namespace AppBundle\Controller;
//use FOS\UserBundle\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="homepage")
     */
     public function indexAction(){
         
     $entityManager = $this->getDoctrine()->getManager();
     $najnowsze = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findBy([], ['updatedAt' => 'DESC']);
        return $this->render('default/home.html.twig', ['memy' => $najnowsze, 'images_dir' => '/images/upload']);
        
        
    }
    /**
     * @Route("/topka", name="topka")
     */
     public function topkaAction(){
     $entityManager = $this->getDoctrine()->getManager();
     $topka = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findBy([], ['votes' => 'DESC']);
        return $this->render('default/topka.html.twig', ['memy' => $topka, 'images_dir' => '/images/upload']);
       
        
    }
    
     /**
     * @Route("/losuj", name="losuj")
     */
     public function losujAction(){
        $em = $this->getDoctrine()->getManager(); // ...or getEntityManager() prior to Symfony 2.1
        $connection = $em->getConnection();
        $statement = $connection->prepare("SELECT * FROM uploads ORDER BY RANDOM() LIMIT 1");
        $statement->execute();
        $result = $statement->fetchAll()[0];
        return $this->render('default/losuj.html.twig', ['mem' => $result, 'images_dir' => '/images/upload']);
        
     }
    
    /**
     * @Route("/dodaj", name="dodaj")
     */
     public function dodajAction(Request $request){
         
        
        if ($request->files->get('imageFile')) {
            
            $up = new \AppBundle\Entity\Uploads();
             
            $up->setImageFile($request->files->get('imageFile'));
            $up->setImageName($request->get('imageName'));
            $up->setImageSize(filesize($request->files->get('imageFile')));
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($up);
            $em->flush();
            
        }
         
         
        return $this->render('default/dodaj.html.twig');
     }
     
     /**
     * @Route("/rejestracja", name="rejestracja")
     */
     public function rejestracjaAction(){
        return $this->render('default/rejestracja.html.twig');
     }
     
     /**
     * @Route("/logowanie", name="logowanie")
     */
     public function logowanieAction(){
        return $this->render('default/logowanie.html.twig');
     }
     
    /**
    * @Route("/memy/{id}", name="mem")
    */
    
    public function memAction($id, Request $request) {
        
        $entityManager = $this->getDoctrine()->getManager();
        $mem = $entityManager->getRepository(\AppBundle\Entity\Uploads::class)->findOneBy(['id' => $id]);
        $voted = false;
        
        if ($request->getMethod() === 'POST') {
            $actual = $mem->getVotes();
            $mem->setVotes($actual + 1);
            $entityManager->persist($mem);
            $entityManager->flush();
            $voted = true;
            
        }
        
        return $this->render('default/mem.html.twig', ['mem' => $mem, 'images_dir' => '/images/upload', 'voted' => $voted]);
    }
    
}
